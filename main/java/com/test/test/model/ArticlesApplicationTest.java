package com.test.test.model;

import com.test.test.repository.ArticlesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class ArticlesApplicationTest {
    @Autowired
    private ArticlesRepository AR;

    @Test
    public void testCreateArticles() {
        Articles a = new Articles("PC", 1300.000, new Date());
        AR.save(a);
    }

    @Test
    public void testFindArticles() {
        Articles a = AR.findById(1L).get();
        System.out.println(a);
    }

    @Test
    public void testUpdateArticles() {
        Articles a = AR.findById(1L).get();
        a.setPrix(100.0);
        AR.save(a);
    }

    @Test
    public void testDeleteArticles() {
        AR.deleteById(1L);
    }

    @Test
    public void testListerTousArticles() {
        List<Articles> art = AR.findAll();
        for (Articles a : art) {
            System.out.println(a);
        }
    }
}
