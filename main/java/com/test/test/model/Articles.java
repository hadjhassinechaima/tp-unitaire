package com.test.test.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;


import java.util.Date;
@Entity
public class Articles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long idArticles;
    private String titre;
    private double prix;
    private Date dateCreation;

    public Articles( String titre, double prix, Date dateCreation) {

        this.titre = titre;
        this.prix = prix;
        this.dateCreation = dateCreation;
    }

    public Articles() {

    }

    public Long getIdArticles() {
        return idArticles;
    }

    public void setIdArticles(Long idArticles) {
        this.idArticles = idArticles;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}
